package star191.f3;

import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;
import java.util.Iterator;

public class CoordHide {

    ArrayList<String> params;

    public CoordHide(){
        params = new ArrayList<String>();

        // hide coords
        params.add("XYZ:");
        params.add("Looking at:");
        params.add("Facing:");
        params.add("Block:");
    }

    @SubscribeEvent
    public void renderOverlayEvent(RenderGameOverlayEvent.Text event){
        // for OPs
        if (FMLClientHandler.instance().getClient().player.capabilities.isCreativeMode){
            return;
        }

//        if (event.isCancelable()) {
//            event.setCanceled(true);
//        }

        Iterator<String> it = event.getLeft().listIterator();

        while (it.hasNext()){
            String value = it.next();

            if (value != null){

                for(String param : params){
                    if (value.startsWith(param)){
                        it.remove();
                    }
                }

            }
        }

    }
}
