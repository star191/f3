package star191.f3;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import org.apache.logging.log4j.Logger;
import star191.f3.proxy.CommonProxy;
import star191.f3.util.R;


@Mod(modid = R.MODID, name = R.NAME, version = R.VERSION)
public class Main
{
    private static Logger logger;

    @Mod.Instance
    public static Main instance;

    @SidedProxy(clientSide = R.CLIENT, serverSide = R.COMMON)
    public static CommonProxy proxy;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        proxy.preInit(event);

        logger = event.getModLog();

    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        proxy.init(event);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }


    // Server
    @EventHandler
    public static void init(FMLServerStartingEvent event)
    {

    }
}
